FROM maven:3.6.3-jdk-11

EXPOSE 8080

COPY . /usr/src/mymaven
WORKDIR /usr/src/mymaven
CMD ["mvn","spring-boot:run"]
