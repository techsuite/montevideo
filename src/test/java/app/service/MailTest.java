package app.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.mail.internet.MimeMessage;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringRunner;

import app.ETL.EmailBody;
import app.ETL.Mail;
import app.factory.SimpleFactory;


@RunWith(SpringRunner.class)
@SpringBootTest
public class MailTest {

  @InjectMocks
  private Mail mail;

  @Mock
  private MimeMessageHelper helper;

  @Mock
  private SimpleFactory simpleFactory;

  @Mock
  private EmailBody emailBody;

  @Mock
  private DataService dataService;

  @Mock
  private JavaMailSender emailSender;

  @Mock
  private MimeMessage mimeMessage;

  @Test
  public void sendSimpleMessageWorks() throws Exception {
    when(emailSender.createMimeMessage()).thenReturn(mimeMessage);
    when(simpleFactory.mimeMessageHelper(any())).thenReturn(helper);

    mail.sendSimpleMessage(
      "montevideo.techsuite@gmail.com",
      "test", emailBody);

    verify(helper).setTo("montevideo.techsuite@gmail.com");
    verify(helper).setSubject("test");
    verify(helper).setText(emailBody.emailBody(dataService));
    verify(emailSender).send(mimeMessage);
  }

}
