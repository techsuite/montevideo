package app.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.logging.Logger;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import app.ETL.ExtractData;
import app.ETL.LoadSidney;
import app.httpRequest.PingCaracas;
import app.model.DataReceive;
import app.model.Enfermedad;
import app.model.Medicamento;
import app.model.Metadata;
import app.model.ProdNat;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.ResponseBody;


@RunWith(SpringRunner.class)
@SpringBootTest
public class DataServiceTest {

    @Autowired
    private PingCaracas pingCaracas;

    @Autowired
    private DataService dataService;

    @MockBean
    private OkHttpClient client;

    @MockBean
    private LoadSidney conexionSidney;

    @Mock
    private Call call;

    @Mock
    private Response response;

    @Mock
    private ResponseBody body;

    @Mock
    private DataReceive data1, data2, data3, data4, data5, data6;

    @Mock
    private Metadata meta1, meta2, meta3, meta4, meta5, meta6;

    @MockBean
    private ExtractData products;

    @MockBean
    private Logger log;

  @Test
  public void sendSidneyAndPingCaracas() throws Exception {
    
    when(client.newCall(any())).thenReturn(call);
        when(call.execute()).thenReturn(response);
        when(response.code()).thenReturn(200);

        pingCaracas.checkCaracasStatus();

        when(client.newCall(any())).thenReturn(call);
        when(call.execute()).thenReturn(response);
        when(response.code()).thenReturn(200);
        when(response.body()).thenReturn(body);
        when(body.string()).thenReturn(" correcto");
    
        conexionSidney.sendDataSidney("test");
  }

  @Test
  public void GetPingCaracasNull(){
      assertThat(dataService.getStatusChecker()).isNotNull();
  }
  @Test
  public void GetSenderNull(){
    assertThat(dataService.getSender()).isNotNull();
}

private Medicamento medicina, medicinaI;
private Enfermedad enfermedad, enfermedadI;
private ProdNat prodNat, prodNatI;
private  Medicamento medicamento[];
private Enfermedad enfermedades[];
private ProdNat prodNats[];
private String productos[]={"medicamentos", "prodNat", "enfermedades"};
private int pageSize;

@Before
public void estadoInicial(){
    medicina= new Medicamento("Ibuprofeno","Posible dolor estomacal","Uno cada 8h",
            "prospecto", "composicion", "url", "español");
    enfermedad= new Enfermedad("VIH","Dolor de cabeza", "Beber ahua con limón",
            "url", "español");
    prodNat= new ProdNat("Zumo de Limón","Posible dolor estomacal","Uno cada 8h",
            "prospecto", "composicion", "url", "español");
    medicinaI= new Medicamento("Ibuprofeno","Posible dolor estomacal","Uno cada 8h",
            "prospecto", "composicion", "url", "english");
    enfermedadI= new Enfermedad("VIH","Dolor de cabeza", "Beber ahua con limón",
            "url", "english");
    prodNatI= new ProdNat("Zumo de Limón","Posible dolor estomacal","Uno cada 8h",
            "prospecto", "composicion", "url", "english");
    medicamento =new Medicamento[1];
    enfermedades =new Enfermedad[1];
    prodNats =new ProdNat[1];
    medicamento[0]=medicina;
    enfermedades[0]=enfermedad;
    prodNats[0]=prodNat;
  
    pageSize = 10;
}

@Test
public void called_dataLoader_WithNoElements_inApi() throws Exception{

    when(products.getData(productos[0], pageSize,1, "spanish")).thenReturn(data1);
    when(data1.getMedicamentos()).thenReturn(medicamento);
    when(data1.getMetadata()).thenReturn(meta1);
    when(meta1.getTotalPages()).thenReturn(1);

    when(products.getData(productos[1], pageSize,1, "spanish")).thenReturn(data2);
    when(data2.getProductos_naturales()).thenReturn(prodNats);
    when(data2.getMetadata()).thenReturn(meta2);
    when(meta2.getTotalPages()).thenReturn(1);

    when(products.getData(productos[2], pageSize,1, "spanish")).thenReturn(data3);
    when(data3.getEnfermedades()).thenReturn(enfermedades);
    when(data3.getMetadata()).thenReturn(meta3);
    when(meta3.getTotalPages()).thenReturn(1);

    when(products.getData(productos[0], pageSize,1, "english")).thenReturn(data4);
    when(data4.getMedicamentos()).thenReturn(medicamento);
    when(data4.getMetadata()).thenReturn(meta4);
    when(meta4.getTotalPages()).thenReturn(1);

    when(products.getData(productos[1], pageSize,1, "english")).thenReturn(data5);
    when(data5.getProductos_naturales()).thenReturn(prodNats);
    when(data5.getMetadata()).thenReturn(meta5);
    when(meta5.getTotalPages()).thenReturn(1);

    when(products.getData(productos[2], pageSize,1, "english")).thenReturn(data6);
    when(data6.getEnfermedades()).thenReturn(enfermedades);
    when(data6.getMetadata()).thenReturn(meta6);
    when(meta6.getTotalPages()).thenReturn(1);

    dataService.service();
}
}
