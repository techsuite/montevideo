package app.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import app.ETL.*;
import app.httpRequest.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PingCaracasTest {

    @Autowired
    private PingCaracas pingCaracas;

    @MockBean
    private OkHttpClient client;

    @MockBean
    private LoadSidney conexionSidney;

    @Mock
    private Call call;

    @Mock
    private Response response;

    @Test
    public void pingCaracas_whenCalled_shouldFinishCorrectly() throws Exception {
        when(client.newCall(any())).thenReturn(call);
        when(call.execute()).thenReturn(response);
        when(response.code()).thenReturn(200);

        pingCaracas.checkCaracasStatus();
    }
}

