package app.service;

import app.ETL.ExtractData;
import app.model.DataReceive;
import okhttp3.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ExtractDataTest {

    String[] pruebasJson = new String[5];

    @Before
    public void crearJsonPruebas() {
        String correctoMedicina = "{" + "\"medicamentos\" : [" + "{" + "\"nombre\":\"nombre1\","
                + "\"contraind\":\"contraindicaciones\"," + "\"posologia\":\"posologia del medicamento\","
                + "\"prospecto\":\"prospecto del medicamento\"," + "\"composicion\":\"composiciones del medicamento\","
                + "\"url\":\"url de la que se ha obtenido la informacion\"," + "\"idioma\":\"spanish\"" + "}" +
                "{" + "\"nombre\":\"nombre2\","
                + "\"contraind\":\"contraindicaciones\"," + "\"posologia\":\"posologia del medicamento\","
                + "\"prospecto\":\"prospecto del medicamento\"," + "\"composicion\":\"composiciones del medicamento\","
                + "\"url\":\"url de la que se ha obtenido la informacion\"," + "\"idioma\":\"spanish\"" + "}" +
                "{" + "\"nombre\":\"nombre3\","
                + "\"contraind\":\"contraindicaciones\"," + "\"posologia\":\"posologia del medicamento\","
                + "\"prospecto\":\"prospecto del medicamento\"," + "\"composicion\":\"composiciones del medicamento\","
                + "\"url\":\"url de la que se ha obtenido la informacion\"," + "\"idioma\":\"spanish\"" + "}" + "]," +

                "\"metadata\" : {" + "\"pageSize\": 10," + "\"pageNum\": 2," + "\"totalPages\": 4" + "}" + "}";
        String correctoEnfermedad = "{" + "\"enfermedades\" : [" + "{" + "\"nombre\":\"nombre1\","
                + "\"sintomas\":\"escozor\"," + "\"recomendaciones\":\"beber agua\","
                + "\"url\":\"url de la que se ha obtenido la informacion\"," + "\"idioma\":\"spanish\"" + "}" + "]," +

                "\"metadata\" : {" + "\"pageSize\": 10," + "\"pageNum\": 2," + "\"totalPages\": 4" + "}" + "}";
        String correctoProductoNatural = "{" + "\"productos_naturales\" : [" + "{" + "\"nombre\":\"nombre1\","
                + "\"contraind\":\"contraindicaciones\"," + "\"posologia\":\"posologia del medicamento\","
                + "\"prospecto\":\"prospecto del medicamento\"," + "\"composicion\":\"composiciones del medicamento\","
                + "\"url\":\"url de la que se ha obtenido la informacion\"," + "\"idioma\":\"spanish\"" + "}" + "]," +

                "\"metadata\" : {" + "\"pageSize\": 10," + "\"pageNum\": 2," + "\"totalPages\": 4" + "}" + "}";
        String incorrecto = "{" + "\"error\" : [" + "{" + "\"nombre\":\"nombre1\","
                + "\"contraind\":\"contraindicaciones\"," + "\"posologia\":\"posologia del medicamento\","
                + "\"prospecto\":\"prospecto del medicamento\"," + "\"composicion\":\"composiciones del medicamento\","
                + "\"url\":\"url de la que se ha obtenido la informacion\"," + "\"idioma\":\"spanish\"" + "}" + "]," + "}";
        String incompleto = "{" + "\"productos_naturales\" : [" + "{" + "\"nombre\":\"nombre1\","
                + "\"posologia\":\"posologia del medicamento\","
                + "\"prospecto\":\"prospecto del medicamento\"," + "\"composicion\":\"composiciones del medicamento\","
                + "\"url\":\"url de la que se ha obtenido la informacion\"," + "\"idioma\":\"spanish\"" + "}" + "]," +

                "\"metadata\" : {" + "\"pageSize\": 10," + "\"pageNum\": 2," + "\"totalPages\": 4" + "}" + "}";
        pruebasJson[0] = correctoMedicina;
        pruebasJson[1] = correctoEnfermedad;
        pruebasJson[2] = correctoProductoNatural;
        pruebasJson[3] = incorrecto;
        pruebasJson[4] = incompleto;
    }

    @Autowired
    private ExtractData ext;

    @MockBean
    private OkHttpClient client;

    @Mock
    private Request request;

    @Mock
    private Response response;

    @Mock
    private Call call;

    @Mock
    private ResponseBody rsbody;


    @Test
    public void getData_whenCalledWithMedicamentos_JsonMistake() throws Exception {
        when(client.newCall(any())).thenReturn(call);
        when(call.execute()).thenReturn(response);
        when(response.isSuccessful()).thenReturn(true);
        when(response.body()).thenReturn(rsbody);
        when(rsbody.string()).thenReturn(pruebasJson[4]);

        DataReceive respuesta = ext.getData(null,0,0,null);
        ext.setClient(ext.getClient());
        assertThat(respuesta).isNotNull();
        assertThat(respuesta.medicamentos).isNull();
       
    }
    @Test
    public void getData_whenCalledEnfermedades_shoulFinishCorrectly() throws Exception {

        when(client.newCall(any())).thenReturn(call);
        when(call.execute()).thenReturn(response);
        when(response.isSuccessful()).thenReturn(true);
        when(response.body()).thenReturn(rsbody);
        when(rsbody.string()).thenReturn(pruebasJson[1]);

        DataReceive respuesta = ext.getData(null, 0, 0, null);
        ext.setClient(ext.getClient());
        assertThat(respuesta).isNotNull();
        assertThat(respuesta.enfermedades).isNotNull();
    }
    @Test
    public void getData_whenCalledWithProductoNatural_shoulFinishCorrectly() throws Exception {

        when(client.newCall(any())).thenReturn(call);
        when(call.execute()).thenReturn(response);
        when(response.isSuccessful()).thenReturn(true);
        when(response.body()).thenReturn(rsbody);
        when(rsbody.string()).thenReturn(pruebasJson[2]);

        DataReceive respuesta = ext.getData( null, 0, 0, null);
        ext.setClient(ext.getClient());
        assertThat(respuesta).isNotNull();
        assertThat(respuesta.productos_naturales).isNotNull();
    }
   
   @Test
    public void getData_whenCalled_ReturnException() throws Exception {

        when(client.newCall(any())).thenReturn(call);
        when(call.execute()).thenReturn(response);
        when(response.isSuccessful()).thenReturn(false);
    }

    /*@Test
    public void getData_whenCalledWithErroneo_shoulReturnDataReceive1() throws Exception {

        when(client.newCall(any())).thenReturn(call);
        when(call.execute()).thenReturn(response);
        when(response.isSuccessful()).thenReturn(true);
        when(response.body()).thenReturn(rsbody);
        when(rsbody.string()).thenReturn(pruebasJson[3]);

        DataReceive respuesta;
        respuesta = ext.getData(null, 0, 0, null);
        assertThat(respuesta).isNull();
    }*/

    @Test
    public void extractDataCoverage() {

    }
}