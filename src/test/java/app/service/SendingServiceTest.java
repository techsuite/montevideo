package app.service;

import static org.mockito.Mockito.when;

import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import app.ETL.ExtractData;
import app.ETL.LoadSidney;
import app.ETL.SendingService;
import app.model.DataReceive;
import app.model.Enfermedad;
import app.model.Medicamento;
import app.model.Metadata;
import app.model.ProdNat;


@RunWith(SpringRunner.class)
@SpringBootTest
    public class SendingServiceTest {
        @Autowired
        private SendingService service;

        @Mock
        private DataReceive data1, data2, data3, data4, data5, data6;

        @Mock
        private Metadata meta1, meta2, meta3, meta4, meta5, meta6;

        @MockBean
        private ExtractData products;
 
        @MockBean
        private LoadSidney loader;
        
        @Mock
        private Logger log;

        //private TransformData dataTransformer;
        private Medicamento medicina;
        private Enfermedad enfermedad;
        private ProdNat prodNat;
        private Medicamento medicamento[];
        private Enfermedad enfermedades[];
        private ProdNat prodNats[];
        private String productos[]={"medicamentos", "prodNat", "enfermedades"};
        private int pageSize;

        @Before
        public void estadoInicial(){

            medicina= new Medicamento("Ibuprofeno","Posible dolor estomacal","Uno cada 8h","prospecto", "composicion", "url", "español");
            enfermedad= new Enfermedad("VIH","Dolor de cabeza", "Beber ahua con limón","url", "español");
            prodNat= new ProdNat("Zumo de Limón","Posible dolor estomacal","Uno cada 8h","prospecto", "composicion", "url", "español");

            medicamento = new Medicamento[1];
            enfermedades = new Enfermedad[1];
            prodNats = new ProdNat[1];

            medicamento[0]=medicina;
            enfermedades[0]=enfermedad;
            prodNats[0]=prodNat;

            pageSize = 10;
        }

        @Test
        public void called_dataLoader_WithNoElements_inApiIfElseElse() throws Exception{

            when(products.getData(productos[0], pageSize,1, "spanish")).thenReturn(data1);
            when(data1.getMedicamentos()).thenReturn(medicamento);

            when(data1.getMetadata()).thenReturn(meta1);
            when(meta1.getTotalPages()).thenReturn(2);

            when(products.getData(productos[1], pageSize,1, "spanish")).thenReturn(data2);
            when(data2.getProductos_naturales()).thenReturn(prodNats);


            when(data2.getMetadata()).thenReturn(meta2);
            when(meta2.getTotalPages()).thenReturn(1);

            when(products.getData(productos[2], pageSize,1, "spanish")).thenReturn(data3);
            when(data3.getEnfermedades()).thenReturn(enfermedades);

            when(data3.getMetadata()).thenReturn(meta3);
            when(meta3.getTotalPages()).thenReturn(1);

            //datapageSize = new DataReceive(medicamento,null,null,null);
            when(products.getData(productos[0], pageSize,2, "spanish")).thenReturn(data1);
            when(data1.getMedicamentos()).thenReturn(medicamento);

            service.dataLoader("spanish");
        }

        @Test
        public void called_dataLoader_WithNoElements_inApi_ElseElseIf() throws Exception{

            when(products.getData(productos[0], pageSize,1, "spanish")).thenReturn(data1);
            when(data1.getMedicamentos()).thenReturn(medicamento);

            when(data1.getMetadata()).thenReturn(meta1);
            when(meta1.getTotalPages()).thenReturn(1);

            when(products.getData(productos[1], pageSize,1, "spanish")).thenReturn(data2);
            when(data2.getProductos_naturales()).thenReturn(prodNats);

            when(data2.getMetadata()).thenReturn(meta2);
            when(meta2.getTotalPages()).thenReturn(1);

            when(products.getData(productos[2], pageSize,1, "spanish")).thenReturn(data3);
            when(data3.getEnfermedades()).thenReturn(enfermedades);

            when(data3.getMetadata()).thenReturn(meta3);
            when(meta3.getTotalPages()).thenReturn(2);

            //data3 = new DataReceive(null,null,enfermedades,null);
            when(products.getData(productos[2], pageSize,2, "spanish")).thenReturn(data3);
            when(data3.getEnfermedades()).thenReturn(enfermedades);

            service.dataLoader("spanish");
        }

        @Test
        public void called_dataLoader_WithNoElements_inApiElseIfElse() throws Exception{

            when(products.getData(productos[0], pageSize,1, "spanish")).thenReturn(data1);
            when(data1.getMedicamentos()).thenReturn(medicamento);

            when(data1.getMetadata()).thenReturn(meta1);
            when(meta1.getTotalPages()).thenReturn(1);

            when(products.getData(productos[1], pageSize,1, "spanish")).thenReturn(data2);
            when(data2.getProductos_naturales()).thenReturn(prodNats);

            when(data2.getMetadata()).thenReturn(meta2);
            when(meta2.getTotalPages()).thenReturn(2);
            //data1 = new DataReceive(null,prodNats,null,null);

            when(products.getData(productos[1], pageSize,2, "spanish")).thenReturn(data2);
            when(data2.getProductos_naturales()).thenReturn(prodNats);

            when(products.getData(productos[2], pageSize,1, "spanish")).thenReturn(data3);
            when(data3.getEnfermedades()).thenReturn(enfermedades);

            when(data3.getMetadata()).thenReturn(meta3);
            when(meta3.getTotalPages()).thenReturn(1);

            service.dataLoader("spanish");
        }

        @Test
        public void emailDataCollectorEnglishS() throws Exception {
     
            when(products.getData(productos[0], pageSize,1, "english")).thenReturn(data1);
            when(data1.getMedicamentos()).thenReturn(medicamento);

            when(data1.getMetadata()).thenReturn(meta1);
            when(meta1.getTotalPages()).thenReturn(1);

            when(products.getData(productos[1], pageSize,1, "english")).thenReturn(data2);
            when(data2.getProductos_naturales()).thenReturn(prodNats);

            when(data2.getMetadata()).thenReturn(meta2);
            when(meta2.getTotalPages()).thenReturn(1);

            when(products.getData(productos[2], pageSize,1, "english")).thenReturn(data3);
            when(data3.getEnfermedades()).thenReturn(enfermedades);

            when(data3.getMetadata()).thenReturn(meta3);
            when(meta3.getTotalPages()).thenReturn(1);

            service.dataLoader("english");
        }
    }
    
