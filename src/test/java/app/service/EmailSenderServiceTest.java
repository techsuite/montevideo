package app.service;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import app.ETL.EmailBody;
import app.ETL.EmailSenderService;
import app.ETL.Mail;
import app.model.MetricasMail;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmailSenderServiceTest {

    @InjectMocks
    private EmailSenderService emailSenderService;

    @Mock
    private MetricasMail metricasEsp, metricasEng;

    @Mock
    private EmailBody emailBody;

    @Mock
    private Mail mail;

    @Mock
    private DataService dataService;
    
    @Test
    public void sendMailWorks() throws Exception {
        when(metricasEsp.getNumEnfermedades()).thenReturn(2);
        when(metricasEsp.getNumMedicamentos()).thenReturn(2);
        when(metricasEsp.getNumProdNat()).thenReturn(2);
        when(metricasEng.getNumEnfermedades()).thenReturn(2);
        when(metricasEng.getNumMedicamentos()).thenReturn(2);
        when(metricasEng.getNumProdNat()).thenReturn(2);
        emailSenderService.sendMail();
        verify(mail).sendSimpleMessage(
            "developer.techsuite@outlook.com", "Informe ETL Montevideo", 
            emailBody);
    }

    @Test
    public void senderServiceExceptionCase() throws Exception {
        doThrow(Exception.class).when(mail).sendSimpleMessage(any(), any(), any());
        emailSenderService.sendMail();
    }

}
