package app.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import app.ETL.EmailBody;
import app.ETL.SendingService;
import app.model.MetricasMail;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmailBodyTest {
    
    @Mock
    private MetricasMail metricasEsp, metricasIng;

    @Mock
    private EmailBody emailBody;

    @Mock
    private DataService dataService;

    @Mock
    private SendingService sender;

    @Test
    public void EmailBodyNull() throws Exception {
        when(dataService.getSender()).thenReturn(sender);
        when(sender.getMetricasEsp()).thenReturn(metricasEsp);
        when(dataService.getSender()).thenReturn(sender);
        when(sender.getMetricasEng()).thenReturn(metricasIng);
        String body = emailBody.emailBody(dataService);
        assertThat(body).isNull();
    }
}
