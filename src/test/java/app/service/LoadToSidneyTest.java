package app.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import app.ETL.LoadSidney;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

@SpringBootTest
@RunWith(SpringRunner.class)

public class LoadToSidneyTest {
    @Autowired
    private LoadSidney load;

    @MockBean
    private OkHttpClient client;

    @Mock
    private Request request;

    @Mock
    private Response response;

    @Mock
    private Call call;

    @Mock
    private ResponseBody rsbody;
    @Test
    public void senData_whenCalled_shouldPost() throws IOException{
        when(client.newCall(any())).thenReturn(call);
        when(call.execute()).thenReturn(response);
        when(response.isSuccessful()).thenReturn(true);
        when(response.body()).thenReturn(rsbody);
        load.sendDataSidney("rsbody");
        verify(call).execute();
        //Habra que verificar que el log de de envio realizado ha sido ejecutado.
    }
    @Test
    public void senData_whenCalled_shouldntPost() throws IOException {
        when(client.newCall(any())).thenReturn(call);
        when(call.execute()).thenReturn(response);
        when(response.isSuccessful()).thenReturn(false);
        when(response.body()).thenReturn(rsbody);
        load.sendDataSidney("rsbody");
        verify(call).execute();
        //Habra que verificar que el log de error ha sido ejecutado.
    }

}
