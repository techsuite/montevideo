package app.service;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.fasterxml.jackson.core.JsonProcessingException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import app.ETL.TransformData;
import app.model.DataReceive;
import app.model.Enfermedad;
import app.model.Medicamento;
import app.model.ProdNat;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class TransformDataTest {
    private Medicamento medicina;

    private Enfermedad enfermedad;

    private ProdNat productonatural;

    private DataReceive data1, data2, data3;

    private TransformData trans=new TransformData();

    @Before
    public void career(){
        /*Se deben crear los distitos arrays introducir los objetos
        * De esa manera se puede construyen los disntintos dataReceive que se
        * suministra como parametros*/
        medicina= new Medicamento("Ibuprofeno","Posible dolor estomacal","Uno cada 8h",
                                    "prospecto", "composicion", "url", "español");
        enfermedad= new Enfermedad("VIH","Dolor de cabeza", "Beber ahua con limón",
                                    "url", "español");
        productonatural= new ProdNat("Zumo de Limón","Posible dolor estomacal","Uno cada 8h",
                "prospecto", "composicion", "url", "español");
        Medicamento medicamento []=new Medicamento[1];
        Enfermedad enfermedades []=new Enfermedad[1];
        ProdNat productos []=new ProdNat[1];
        medicamento[0]=medicina;
        enfermedades[0]=enfermedad;
        productos[0]=productonatural;

        data1=new DataReceive();
        data2=new DataReceive();
        data3=new DataReceive();

        data1.setMedicamentos(medicamento);
        data2.setEnfermedades(enfermedades);
        data3.setProductos_naturales(productos);
    }

    @Test
    public void datas_shouldBe_empty() throws  JsonProcessingException{
        assertNull(data2.getMedicamentos());
        assertNull(data1.getEnfermedades());
        assertNull(data3.getEnfermedades());
    }
    @Test
    public void callToTransformaDato_shouldReturnAstring() throws  JsonProcessingException{
        assertThat(trans.transformaDato(data1,data2,data3)).asString();
    }
}
