package app.model;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

public class DataRecieveTest {
    private DataReceive dat1, dat2;
    @Before
    public void inicializacion_objetos(){
        Enfermedad enfermedad []= new Enfermedad[1];
        Medicamento medicamento[]=new Medicamento[1];
        ProdNat p[]=new ProdNat[1];
        p[0]=new ProdNat();
        medicamento[0]=new Medicamento();
        enfermedad[0]=new Enfermedad();
        Metadata m = new Metadata();
        m.setPageNum(1);
        m.setPageSize(2);
        m.toString();
        m.hashCode();
        //Metadata met = new Metadata(1,2,3);
        dat1 = new DataReceive();
        dat2 = new DataReceive(medicamento,p,enfermedad,m);
    }

    @Test
    public void DataReceive_Coverage(){
        assertThat(dat2).isNotNull();
        dat1.setEnfermedades(dat2.getEnfermedades());
        dat1.setMedicamentos(dat2.getMedicamentos());
        dat1.setProductos_naturales(dat2.getProductos_naturales());
        dat1.hashCode();
        dat1.toString();
    }
}
