package app.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MetricasMailTest {

    private MetricasMail metricas1, metricas2;

    @Before
    public void inicializacion_objetos(){
        metricas1 = new MetricasMail();
        metricas2 = new MetricasMail(1,2,3);
    }
    @Test
    public void classEnfermedad_shlouldWorks_Well(){
        assertThat(metricas2).isNotNull();
        metricas1.setNumEnfermedades(2);
        metricas1.setNumMedicamentos(3);
        metricas1.setNumProdNat(5);
        metricas1.getNumEnfermedades();
        metricas1.toString();
        metricas1.hashCode();
        assertThat(metricas1).isNotNull();
        assertThat(metricas1.getNumEnfermedades()).asString();
    }
}
