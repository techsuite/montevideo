package app.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)

public class MedicamentoTest {
    private Medicamento medicamento1;
    private Medicamento medicamento2;

    @Before
    public void inicializacion_objetos(){
        medicamento1 = new Medicamento();
        medicamento2 = new Medicamento("a","b", "c","d", "e","f","h");
    }
    @Test
    public void classEnfermedad_shlouldWorks_Well(){
        assertThat(medicamento2).isNotNull();
        medicamento1.setIdioma("a");
        medicamento1.setNombre("a");
        medicamento1.setPosologia("a");
        medicamento1.setContraind("a");
        medicamento1.setProspecto("a");
        medicamento1.toString();
        medicamento1.hashCode();
        assertThat(medicamento1).isNotNull();
        assertThat(medicamento1.getIdioma()).asString();
    }
}
