package app.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class EnfermedadTest {

    private Enfermedad enfermedad1;
    private Enfermedad enfermedad2;

    @Before
    public void inicializacion_objetos(){
        enfermedad1 = new Enfermedad();
        enfermedad2 = new Enfermedad("a","b", "c","d", "e");
    }
    @Test
    public void classEnfermedad_shlouldWorks_Well(){
        assertThat(enfermedad2).isNotNull();
        enfermedad1.setIdioma("a");
        enfermedad1.setNombre("a");
        enfermedad1.setRecomendaciones("a");
        enfermedad1.getSintomas();
        enfermedad1.toString();
        enfermedad1.hashCode();
        assertThat(enfermedad1).isNotNull();
        assertThat(enfermedad1.getIdioma()).asString();
    }
}
