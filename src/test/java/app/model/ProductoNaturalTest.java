package app.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)

public class ProductoNaturalTest {
    private ProdNat producto_natural1;
    private ProdNat producto_natural2;

    @Before
    public void inicializacion_objetos() {
        producto_natural1 = new ProdNat();
        producto_natural2 = new ProdNat("a", "b", "c", "d", "e", "f", "h");
    }

    @Test
    public void classProdNat_shlouldWorks_Well() {
        assertThat(producto_natural2).isNotNull();
        producto_natural1.setIdioma("a");
        producto_natural1.setNombre("a");
        producto_natural1.setPosologia("a");
        producto_natural1.setContraind("a");
        producto_natural1.setProspecto("a");
        producto_natural1.toString();
        producto_natural1.hashCode();
        assertThat(producto_natural1).isNotNull();
        assertThat(producto_natural1.getIdioma()).asString();
    }
}