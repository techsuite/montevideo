package app.model;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor

public class Enfermedad {
    public String nombre;
    public String sintomas;
    public String recomendaciones;
    public String url;
    public String idioma; 
}
