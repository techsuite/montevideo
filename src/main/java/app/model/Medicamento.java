package app.model;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor

public class Medicamento {
    public String nombre;
    public String contraind;
    public String posologia;
    public String prospecto;
    public String composicion;
    public String url;
    public String idioma; 
}

