package app.model;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor

public class DataSend{
    public String key = "Fl9z%oJu#pLb";
    public Medicamento[] medicamentos;
    public Enfermedad[] enfermedades;
    public ProdNat[] productos_naturales;

}
