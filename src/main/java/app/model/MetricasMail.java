package app.model;

import org.springframework.stereotype.Service;
import lombok.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Service
public class MetricasMail {
    public int numMedicamentos;
    public int numProdNat;
    public int numEnfermedades;
}