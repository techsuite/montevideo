package app.model;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor

public class Metadata {

    public int pageSize;
    public int pageNum;
    public int totalPages;
    
}
