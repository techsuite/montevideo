package app.model;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor


public class DataReceive{

     public Medicamento [] medicamentos;
     public ProdNat [] productos_naturales;
     public Enfermedad [] enfermedades;
     public Metadata metadata;

}
