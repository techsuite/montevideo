package app.controller;

import app.ETL.SendingService;
import app.model.Busqueda;
import app.model.MetricasMail;
import app.service.DataService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ViewController {

  @Autowired
  SendingService service;
  @Autowired
  DataService dataService;

  @GetMapping("/home")
  public String home(Model model) throws Exception {
    model.addAttribute(
      "busquedas",
      List.of(
        new Busqueda(1, "test 1", 100L),
        new Busqueda(2, "prueba 2", 300L)
      )
    );
    model.addAttribute("mensaje", "hola desde spring boot");
    return "home";
  }

  @GetMapping("/metricas")
  public String metricas(Model model) throws Exception {

    MetricasMail metricasEsp;
    MetricasMail metricasIng;

    metricasEsp = dataService.getSender().metricasEsp;
    metricasIng = dataService.getSender().metricasEng;
    

    model.addAttribute("EnfEsp", metricasEsp.getNumEnfermedades());
    model.addAttribute("ProdNatEsp", metricasEsp.getNumProdNat());
    model.addAttribute("MedEsp", metricasEsp.getNumMedicamentos());

    model.addAttribute("EnfIng", metricasIng.getNumEnfermedades());
    model.addAttribute("ProdNatIng", metricasIng.getNumProdNat());
    model.addAttribute("MedIng", metricasIng.getNumMedicamentos());

    return "metricas";
  }
}