package app.config;

import org.apache.commons.dbutils.QueryRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import okhttp3.OkHttpClient;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import org.springframework.web.filter.AbstractRequestLoggingFilter;

import java.util.Properties;

@Configuration
public class BeanConfiguration {

  //Configuracion del bean para crear el cliente
  @Bean
  public OkHttpClient createOkHttpClient() {
    return new OkHttpClient();
  }

  @Bean
  public QueryRunner queryRunner() {
    return new QueryRunner();
  }

  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
      .select()
      .apis(RequestHandlerSelectors.basePackage("app.controller"))
      .paths(PathSelectors.any())
      .build();
  }
  
  @Bean
  public AbstractRequestLoggingFilter logFilter() {
    return new CustomLogsFilter();
  }

  @Bean
  public JavaMailSenderImpl getJavaMailSender1()
  {
    JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
    mailSender.setHost("smtp.gmail.com");
    mailSender.setPort(587);

    mailSender.setUsername("montevideo.techsuite@gmail.com");
    mailSender.setPassword("montevideo123A");

    Properties props = mailSender.getJavaMailProperties();
    props.put("mail.transport.protocol", "smtp");
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.starttls.enable", "true");
    props.put("mail.debug", "true");

    return mailSender;
  }
}