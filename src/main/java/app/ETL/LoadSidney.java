package app.ETL;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import okhttp3.*;
import org.slf4j.*;


@Getter
@Setter
@Service
@NoArgsConstructor
@AllArgsConstructor
public class LoadSidney {

@Autowired
private OkHttpClient client;
private static Logger logController = LoggerFactory.getLogger(LoadSidney.class);

public void sendDataSidney(String json) throws IOException {

    //Creamos el json en el formato especificado con Sidney

    RequestBody body = RequestBody.create(json, MediaType.parse("application/json; charset=utf-8"));
    Request request = new Request.Builder().url("https://sidney.juliocastrodev.duckdns.org/getDataMontevideo").post(body).build();

    Call call = client.newCall(request);
    Response response = call.execute();

    //Si la respuesta no es la esperada se notifica por pantalla
    if(!response.isSuccessful()){

        System.out.println("ERROR, code:"+response.code()+response.body().string());
        logController.error("======= ERROR EN EL ENVIO. CODIGO ERROR " + response +" =======");
        }
    
    response.close();

    }
}
