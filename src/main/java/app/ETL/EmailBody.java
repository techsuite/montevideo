package app.ETL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.model.MetricasMail;
import app.service.DataService;

@Service
public class EmailBody {

    @Autowired
    private MetricasMail metricasEsp, metricasEng;

    public String emailBody(DataService dataService) {
        metricasEsp = dataService.getSender().metricasEsp;
        metricasEng = dataService.getSender().metricasEng;
        String body = "Informe de recepción y envío de enfermedades, productos naturales y medicamentos:\n\n"+
                "El total de medicamentos en español es: "+metricasEsp.getNumMedicamentos()+"\n"+
                "El total de productos naturales en español es: "+metricasEsp.getNumProdNat()+"\n"+
                "El total de enfermedades en español es: "+metricasEsp.getNumEnfermedades()+"\n"+
                "El total de medicamentos en inglés es: "+metricasEng.getNumMedicamentos()+"\n"+
                "El total de productos naturales en inglés es: "+metricasEng.getNumProdNat()+"\n"+
                "El total de enfermedades en inglés es: "+metricasEng.getNumEnfermedades()+"\n";
        return body;
      }
}
