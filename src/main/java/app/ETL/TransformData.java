package app.ETL;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.stereotype.Service;

import app.model.DataReceive;
import app.model.DataSend;

@Service

public class TransformData {

    public String transformaDato(DataReceive medicamentos,DataReceive productos_naturales, DataReceive enfermedades) throws JsonProcessingException{
        
        DataSend result = new DataSend();

        if(medicamentos != null)
        result.setMedicamentos(medicamentos.getMedicamentos()); 

        if(productos_naturales != null)
        result.setProductos_naturales(productos_naturales.getProductos_naturales());

        if(enfermedades != null)
        result.setEnfermedades(enfermedades.getEnfermedades());

        ObjectMapper mapper = new ObjectMapper();
        String jsonString = mapper.writeValueAsString(result);
        return jsonString;
    }
    
}
