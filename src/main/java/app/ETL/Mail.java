package app.ETL;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import app.factory.SimpleFactory;
import app.service.DataService;

@Service
public class Mail {

    @Autowired
    private SimpleFactory simpleFactory;

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private DataService dataService;

    public void sendSimpleMessage(String to, String subject, EmailBody emailBody) throws Exception {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = simpleFactory.mimeMessageHelper(message);
        message.setFrom("montevideo.techsuite@gmail.com");
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(emailBody.emailBody(dataService));
        emailSender.send(message);        
      }
}
