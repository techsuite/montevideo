package app.ETL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import app.model.DataReceive;
import app.model.MetricasMail;
import lombok.Getter;

@Service
@Getter
public class SendingService{

    @Autowired
    LoadSidney dataSender;

    @Autowired
    ExtractData products;

    @Autowired
    public
    MetricasMail metricasEsp;

    @Autowired
    public
    MetricasMail metricasEng;

    private static Logger logController = LoggerFactory.getLogger(SendingService.class);

    public MetricasMail[] dataLoader(String language) throws Exception{

            TransformData transformData = new TransformData();

            String[] productos = {"medicamentos","prodNat","enfermedades"};
            String json;
            //Si se cambia entoces tambien se cambia en el test
            //Se cambia en SendingServiceTest y DataServiceTest
            int pageSize=10;
            int pageNumber=1;

            logController.info("======= INICIO EXTRACCION PAGINA 1 =======");

            int totalPagesMedicamentos = products.getData(productos[0], pageSize, pageNumber, language).getMetadata().getTotalPages();
            int totalPagesProdNat = products.getData(productos[1], pageSize, pageNumber, language).getMetadata().getTotalPages();
            int totalPagesEnfermedades = products.getData(productos[2], pageSize, pageNumber, language).getMetadata().getTotalPages();
            int maxPags = Math.max(totalPagesEnfermedades, Math.max(totalPagesMedicamentos, totalPagesProdNat));

            logController.info("======= EXTRACCION PAGINA 1 COMPLETADA =======");

            DataReceive dataReceiveMedicamentos = products.getData(productos[0], pageSize, pageNumber, language);
            DataReceive dataReceiveProductosNat = products.getData(productos[1], pageSize, pageNumber, language);
            DataReceive dataReceiveEnfermedades = products.getData(productos[2], pageSize, pageNumber, language);

            int contMedica = 0, contEnfer = 0, contProdNat = 0;

            for(;pageNumber <= maxPags;pageNumber++){

                logController.info("======= INICIO EXTRACCION PAGINA " + pageNumber + " DE " + maxPags+ " ======= ");

                if(pageNumber <= totalPagesMedicamentos){

                    dataReceiveMedicamentos = products.getData(productos[0], pageSize, pageNumber, language);

                    if(dataReceiveMedicamentos.getMedicamentos() != null)
                        contMedica += dataReceiveMedicamentos.getMedicamentos().length;

                }else{

                    dataReceiveMedicamentos = null;
                }

                if(pageNumber <= totalPagesProdNat){

                    dataReceiveProductosNat = products.getData(productos[1], pageSize, pageNumber, language);
                    
                    if(dataReceiveProductosNat.getProductos_naturales() != null)
                        contProdNat += dataReceiveProductosNat.getProductos_naturales().length;

                }else{

                    dataReceiveProductosNat = null; 
                }

                if(pageNumber <= totalPagesEnfermedades){

                    dataReceiveEnfermedades = products.getData(productos[2], pageSize, pageNumber, language);
                    
                    if(dataReceiveEnfermedades.getEnfermedades() != null)
                        contEnfer += dataReceiveEnfermedades.getEnfermedades().length;

                }else{

                    dataReceiveEnfermedades = null;
                }

                logController.info("======= EXTRACCION PAGINA " + pageNumber + " DE " + maxPags+ " COMPLETADA ======= ");

                logController.info("======= INICIO TRANSFORMACION PAGINA " + pageNumber + " DE " + maxPags+ " ======= ");

                json = transformData.transformaDato(dataReceiveMedicamentos, dataReceiveProductosNat, dataReceiveEnfermedades);

                logController.info("======= TRANSFORMACION PAGINA " + pageNumber + " DE " + maxPags+ " COMPLETADA ======= ");

                logController.info("======= INICIO ENVIO SIDNEY PAGINA " + pageNumber + " DE " + maxPags+ " ======= ");

                dataSender.sendDataSidney(json);

                logController.info("======= ENVIO PAGINA " + pageNumber + " DE " + maxPags+ " COMPLETADA ======= ");

            }

            if(language.equals("spanish")){

                metricasEsp = new MetricasMail( contMedica, contProdNat,contEnfer);

            }else{

                metricasEng = new MetricasMail(contMedica, contProdNat,contEnfer);

            }   

            MetricasMail resMetricas[] = new MetricasMail[2];
            resMetricas[0] = metricasEng;
            resMetricas[1] = metricasEsp;
            
            return resMetricas;
        }
    }