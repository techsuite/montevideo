package app.ETL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
@Service
public class EmailSenderService {

    @Autowired
    private EmailBody emailBody;

    @Autowired
    private Mail mail;

    @Scheduled(cron = "0 0 */1 * * *")
    public void sendMail() {
      try {
        mail.sendSimpleMessage("developer.techsuite@outlook.com", "Informe ETL Montevideo", emailBody);
      } catch (Exception e) {
          e.printStackTrace();
      }
    }
}