package app.ETL;

import com.google.gson.GsonBuilder;
import okhttp3.*;
import java.io.IOException;
import com.google.gson.Gson;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import app.model.*;
import lombok.*;

@Getter
@Setter
@Service
@AllArgsConstructor
@NoArgsConstructor

public class ExtractData {

    @Autowired
    private OkHttpClient client;
    private static Logger logController = LoggerFactory.getLogger(ExtractData.class);

    public  DataReceive getData(String nameProducto,int pageSize,int pageNumber,String idioma) throws IOException {
        
        Request request = new Request.Builder().url("https://caracas.juliocastrodev.duckdns.org/"+nameProducto+"?pageSize=" + pageSize + "&pageNumber=" + 
        pageNumber + "&idioma=" + idioma).build();
        Response response = client.newCall(request).execute();
        String json = response.body().string();

        if (!response.isSuccessful()) {
            
            //DataReceive medica = new Gson().fromJson(json, DataReceive.class);
            //medica.metadata.totalPages = 0;
            logController.error("======= ERROR EN LA EXTRACCION. CODIGO DE ERROR " + response + " =======");
            return new DataReceive (null,null,null,new Metadata (0,0,0));

            //throw new IOException("Error in the response: " + response);

        } else {

            DataReceive medica = new GsonBuilder().setPrettyPrinting().create().fromJson(json, DataReceive.class);
            response.close();
            return medica;
        }
    }

}