package app.httpRequest;

import okhttp3.*;

import org.slf4j.*;
import org.springframework.stereotype.Service;

@Service
public class PingCaracas {
    
private static Logger logController = LoggerFactory.getLogger(PingCaracas.class);

public void checkCaracasStatus() throws java.io.IOException {

    //Hacemos la llamada a /status de caracas para comprobar el estado del nodo
    OkHttpClient client = new OkHttpClient();
    Request request = new Request.Builder().url("https://caracas.juliocastrodev.duckdns.org/status").build();
    Response response = client.newCall(request).execute();

    //Si la respuesta no es la esperada se imprime un mensaje que lo notifica
    if (!response.isSuccessful()) {
        
        logController.error("Connection failed, error code: " + response);
    }
    response.close();
    }
}
