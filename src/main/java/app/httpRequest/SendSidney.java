package app.httpRequest;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import okhttp3.*;

@Service
public class SendSidney {

@Autowired
private OkHttpClient client;

public void sendDataSidney(int data) throws IOException {

    //Creamos el json en el formato especificado con Sidney
    String json = "{\"senderID\":\"montevideo\",\"number\":"+data+"}";

    RequestBody body = RequestBody.create(json, MediaType.parse("application/json"));
    Request request = new Request.Builder().url("https://sidney.juliocastrodev.duckdns.org/getNumb").post(body).build();

    Call call = client.newCall(request);
    Response response = call.execute();

    //Si la respuesta no es la esperada se notifica por pantalla
    if(!response.isSuccessful()){
        System.out.println("ERROR, code:"+response.code()+response.body().string());
        }
    }
}