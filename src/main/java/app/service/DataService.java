package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import lombok.AllArgsConstructor;
import lombok.Getter;
import app.ETL.*;
import app.httpRequest.*;

@AllArgsConstructor
@Service
@Getter

public class DataService {
    @Autowired
    PingCaracas statusChecker = new PingCaracas();
    @Autowired
    SendingService sender = new SendingService();
    
    //Se ejecuta cada X
    @Scheduled(cron = "0 0 */1 * * *")
    public void service() throws Exception{

        statusChecker.checkCaracasStatus();
        sender.dataLoader("spanish");
        sender.dataLoader("english");

    }
}
